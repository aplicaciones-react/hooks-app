import React, { useState } from 'react';
import { MultipleCustomHook } from '../03-examples/MultipleCustomHook';
import '../02-useEffect/effect.css';


export const RealExampleRef = () => {

    const [show, setShow] = useState(true)

    return (
        <>
            <h1>Real Example Ref</h1>
            <hr/>

            {show && <MultipleCustomHook />}

            <button className="btn btn-primary mt-5" onClick = { () => {
                setShow(!show)
            } }>
                Show/Hide
            </button>
        </>
    )
}
