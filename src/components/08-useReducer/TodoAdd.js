import React from 'react'
import { useForm } from '../../hooks/useForm'

export const TodoAdd = ({ handleAddTodo }) => {

    //Llamar al hook useForm para hacer reactivo el formulario
    const [{ description }, handleInputChange, reset] = useForm({
        description: '' //Este es el nombre de la caja de texto que se envia al useForm
    })

    const handleSubmit = (event) => {
        event.preventDefault();
        
        //Validar que el campo description no este vacio
        if (description.trim().length <= 1) {
            return;
        }

        //Nuevo esado
        const newTodo = {
            id: new Date().getTime(),
            desc: description,
            done: false
        };


        handleAddTodo(newTodo);
        //Limpiar el formulario, este metodo esta en el useForm
        reset();

    }

    return (
        <>
            <h4>Agregar TODO</h4>
            <hr />
            <form onSubmit={handleSubmit}>
                <input
                    type="text"
                    name="description"
                    placeholder="Aprender..."
                    autoComplete="off"
                    className="form-control"
                    value={description}
                    onChange={handleInputChange}
                />
                <button
                    type="submit"
                    className="btn btn-outline-success mt-1 btn-block">
                    Agregar
                </button>
            </form>
        </>
    )
}
