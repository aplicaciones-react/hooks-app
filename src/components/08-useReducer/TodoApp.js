import React, { useReducer, useEffect } from 'react';
import { todoReducer } from './todoReducer';
import { TodoList } from './TodoList';
import { TodoAdd } from './TodoAdd';

import './styles.css';

//Funcion que inicializa el useReducer
const init = () => {
    //Si existe lolocalStorage.getItem('todos') lo convierte a json, si es null retorna arreglo vacio
    return JSON.parse(localStorage.getItem('todos')) || []; 

    // return [{
    //     id: new Date().getTime(),
    //     desc: 'Aprender React',
    //     done: false
    // }]
}

export const TodoApp = () => {


    const [ todos, dispatch ] = useReducer(todoReducer, [], init);


    // Cuando la variable "todos" cambie, se va a ejecutar el useEffect
    useEffect(() => {
        localStorage.setItem('todos', JSON.stringify(todos))
    }, [todos]);

    //Funcion que elimina un Todo
    const handleDelete = (todoId) =>{
        const action = {
            type: "delete",
            payload: todoId
        }

        dispatch(action);
    }

    const handleToggle = (todoId) =>{
        dispatch({
            type: "toggle",
            payload: todoId
        });
    }

    const handleAddTodo = ( newTodo ) =>{
        //Disparar la accion en el useReducer
        dispatch({
            type: "add",
            payload: newTodo
        });
    }

     return (
        <>
            <h1>TodoApp  <small>({todos.length})</small></h1>
            <hr/>

            <div className="row">
                <div className="col-7">
                    <TodoList 
                        todos        = {todos}
                        handleDelete = {handleDelete}
                        handleToggle = {handleToggle}
                    />
                </div>
                <div className="col-5">
                    <TodoAdd handleAddTodo={handleAddTodo} />
                </div>
            </div>
        </>
    )
}
