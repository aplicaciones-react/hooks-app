import React, { useEffect, useState } from 'react';
import { Message } from './Message';
import './effect.css'

export const SimpleForm = () => {

    const [formState, setFormState] = useState({
        name : '',
        email: '',
    });

    const { name, email } = formState;

    useEffect( () => {
        console.log("dfdfdf")
    }, []); //Se le pasa arreglo vacio para indicar que se ejecute 1 vez al cargar el componente

    const handleInputChange = ({target}) => {
        setFormState({
            ...formState,
            [target.name] : target.value
        })
    }

    return (
      <>
        <h1>useEffect</h1>
        <hr />

        <div className = "form-group">
          <input
            type         = "text"
            name         = "name"
            className    = "form-control"
            placeholder  = "Tu nombre"
            autoComplete = "off"
            value        = {name}
            onChange     = {handleInputChange}
          />
        </div>

        <div className = "form-group">
          <input
            type         = "text"
            name         = "email"
            className    = "form-control"
            placeholder  = "Tu correo"
            autoComplete = "off"
            value        = {email}
            onChange     = {handleInputChange}
          />
        </div>

        { (name === '123') && <Message /> }
      </>
    );
}
