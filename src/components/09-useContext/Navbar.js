import React from 'react';
import { Link, NavLink } from 'react-router-dom';

export const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            <Link to="/" className="navbar-brand">UseContext</Link>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink exact to="/" activeClassName="active" className="nav-link">Home</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact to="./about" activeClassName="active" className="nav-link">About</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact to="./login" activeClassName="active" className="nav-link">Login</NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    )
}
