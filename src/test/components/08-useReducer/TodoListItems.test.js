import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';
import '../../../setupTest';
import { TodoListItems } from '../../../components/08-useReducer/TodoListItems';
import { demoTodos } from "../../fixtures/demoTodos";


describe('Pruebas en <TodoListItems />', () => {
    
    const handleDelete = jest.fn();
    const handleToggle = jest.fn();

    const wrapper = shallow(
        <TodoListItems 
            todo = {demoTodos[0]}
            index = { 0 }
            handleDelete = {handleDelete}
            handleToggle = {handleToggle}
        />
    )

    test('debe de mostrarce correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    });
    
    test('debe de llamar la funcion handleDelete', () => {

        wrapper.find('button').simulate('click');
        expect(handleDelete).toHaveBeenCalledWith(demoTodos[0].id);

    });

    test('debe de llamar la funcion handleToggle', () => {

        wrapper.find('p').simulate('click');
        expect(handleToggle).toHaveBeenCalledWith(demoTodos[0].id);

    });

    test('debe de mostrar el texto correctamente', () => {
        
        const p = wrapper.find('p');
        expect(p.text().trim()).toBe(`1. ${demoTodos[0].desc}`);

    });

    test('Debe de tener la clase .complete si el todo.done esta en true', () => {
        
        const todo = demoTodos[0];
        todo.done = true;

        const wrapper = shallow(
            <TodoListItems
                todo={todo}
            />
        )
        expect(wrapper.find('p').hasClass('complete')).toBe(true);
    })
    
})

