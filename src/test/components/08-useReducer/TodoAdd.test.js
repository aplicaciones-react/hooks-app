import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';
import '../../../setupTest';
import { TodoAdd } from '../../../components/08-useReducer/TodoAdd';

describe('Pruebas en <TodoAdd />', () => {

    const handleAddTodo = jest.fn();
    
    const wrapper = shallow(
        <TodoAdd
            handleAddTodo={handleAddTodo}
        />
    );

    test('Debe mostrarce correctamente', () => {
        
        expect(wrapper).toMatchSnapshot();

    });
    
    test('No debe de llamar handleAddTodo', () => {

        const formSubmit = wrapper.find('form').prop('onSubmit');
        
        formSubmit({ preventDefault(){} })

        expect(handleAddTodo).toHaveBeenCalledTimes(0); //Que no haya sido llamado

    });

    test('Debe de llamar la funcion handleAddTodo', () => {
        
        const value = 'Prender React';

        //Simular un valor al input
        wrapper.find('input').simulate('change',{
            target:{
                value,
                name: 'description'
            }
        });

        const formSubmit = wrapper.find('form').prop('onSubmit');

        formSubmit({ preventDefault() { } })

        expect(handleAddTodo).toHaveBeenCalledTimes(1);
        expect(handleAddTodo).toHaveBeenCalledWith({
            id: expect.any(Number),
            desc: value,
            done: false,
        });

        expect(wrapper.find('input').prop('value')).toBe('');

    })
    

})
