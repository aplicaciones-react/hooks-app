import React from 'react';
import {shallow} from 'enzyme';
import '@testing-library/jest-dom';
import '../../../setupTest';
import { MultipleCustomHook } from '../../../components/03-examples/MultipleCustomHook';
import { useFetch } from '../../../hooks/useFetch';
import { useCounter } from '../../../hooks/useCounter';

//Cuando se use en este archivo se usaran estos datos falsos
jest.mock('../../../hooks/useFetch');
jest.mock('../../../hooks/useCounter');

describe('Pruebas en MultipleCustomHook', () => {

    useCounter.mockReturnValue({
        counter:10,
        increment:() => {}
    });
   
    test('debe de mostrarce correctamente ', () => {
        
        useFetch.mockReturnValue({
            data:null,
            loading:true,
            error:null
        });

        const wrapper = shallow( <MultipleCustomHook /> );

        expect(wrapper).toMatchSnapshot();
    });

    test('debe de mostrar la información', () => {
        
        useFetch.mockReturnValue({
            data: [{
                author:'Enmanuel',
                quote:'hola you'
            }],
            loading: false,
            error: null
        });

        const wrapper = shallow(<MultipleCustomHook />);

        expect(wrapper.find('.alert').exists()).toBe(false); //No deberia existir la clase alert en el componente
        expect(wrapper.find('.mb-0').text().trim()).toBe('hola you'); //el texto debe ser hola you
        expect(wrapper.find('footer').text().trim()).toBe('Enmanuel'); //el texto del footer debe ser Enmanuel

        // console.log(wrapper.html())

    })
    
        
})
